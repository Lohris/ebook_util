const fs = require("fs");
const OpenCC = require('opencc');
const inputFolder = "./input";
const tmpFolder = "./tmp";
const AdmZip = require('adm-zip');
const path = require("path");
const outputFolder = "./output";
const kindlegen = require('kindlegen');

kindlegen(fs.readFileSync("/result.epub"), (error, mobi) => {
			// mobi is an instance of Buffer with the compiled mobi file
			fs.writeFileSync("./result.mobi",mobi);
		});		