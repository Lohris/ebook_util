const fs = require("fs");
const OpenCC = require('opencc');
const inputFolder = "./input";
const tmpFolder = "./tmp";
const AdmZip = require('adm-zip');
const path = require("path");
const outputFolder = "./output";
if(fs.exist(outputFolder)){
	fs.rmdirSync(outputFolder, {
		recursive: true
	});

}
fs.mkdirSync(outputFolder);

//read a zip from input folder
const extractionDone = new Promise((resolve, reject) => {
	fs.readdir(inputFolder, (err, files) => {
		files.forEach(file => {
			if (file.endsWith(".epub")) {
				var zip = new AdmZip(inputFolder + "/" + file);
				zip.extractAllTo(tmpFolder, true);
				resolve(true);
			}
		});
	});
});



function convertAllFilesCNtoZH(dir, target) {
	var files = fs.readdirSync(dir);
	for (var file of files) {
		let originalFullPath = path.join(dir, file);
		let targetFullPath = path.join(target, file);
		if (fs.lstatSync(originalFullPath).isDirectory()) {
			fs.mkdirSync(targetFullPath);
			convertAllFilesCNtoZH(originalFullPath, targetFullPath);
		} else {
			var data = fs.readFileSync(originalFullPath, {
				encoding: 'utf8',
				flag: 'r'
			});

			if (originalFullPath.endsWith(".xhtml")) {
				const converter = new OpenCC('s2t.json');
				data = converter.convertSync(data);
			}
			fs.writeFileSync(targetFullPath, data);
		}
	};
}


//converting ch to zh for each file inside the zip 
extractionDone.then((result) => {
	convertAllFilesCNtoZH(tmpFolder, outputFolder);
	var targetZip = new AdmZip();
	targetZip.addLocalFolder(outputFolder);
	targetZip.writeZip("./result.epub",(target)=>{
		console.log(target);
	});
				fs.rmdirSync(tmpFolder, {
				recursive: true
			});
			fs.mkdirSync(tmpFolder);

}, (err) => {

});



//re-zip the file and output to output folder

//move the input file to processed file